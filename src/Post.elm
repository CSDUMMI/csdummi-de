module Post exposing
  ( Post
  , empty
  , read
  , view
  , get
  , get_all
  , decoder
  )
import Http
import Element exposing (Element,el, text, paragraph, html)
import Element.Font as Font
import Element.Events as Events
import Url.Builder
import Markdown
import Json.Decode as D

type alias Post =
  { title : String
  , content : String
  , author : String
  , publishing_date : String
  , id : Int
  }

empty : Post
empty =
  { title = ""
  , content = ""
  , author = ""
  , publishing_date = ""
  , id = 0
  }

read : Post -> Element msg
read post
  = paragraph
      [ Element.centerX
      , Font.family
          [ Font.monospace
          ]
      , Font.center
      ]
      [ el [Font.bold] <| text post.title
      , html <| Markdown.toHtml [] post.content
      , text (post.author ++ " " ++ post.publishing_date)
      ]

view : (Post -> msg) -> Post -> Element msg
view on_click post
  = paragraph
      [ Element.centerX
      , Font.family
          [ Font.monospace ]
      , Font.center
      ]
      [ el [Font.bold, Events.onClick (on_click post)] <| text post.title
      , post.content
        |> String.lines
        |> List.take 10
        |> String.join "\n"
        |> Markdown.toHtml []
        |> html
      , text (post.author ++ " " ++ post.publishing_date)
      ]

get : (Result Http.Error Post -> msg) -> Int -> Cmd msg
get msg id =
  Http.get
    { url = Url.Builder.absolute ["api","v0","read", String.fromInt id] []
    , expect = Http.expectJson msg decoder
    }

get_all : (Result Http.Error (List Post) -> msg) -> Cmd msg
get_all msg =
  Http.get
    { url = Url.Builder.absolute ["api", "v0", "feed"] []
    , expect = Http.expectJson msg <| D.list decoder
    }

decoder : D.Decoder Post
decoder =
  D.map5 Post
    (D.field "title" D.string)
    (D.field "content" D.string)
    (D.field "author" D.string)
    (D.field "publishing_date" D.string)
    (D.field "id" D.int)
