module Main exposing (..)
import Browser
import Http
import Post exposing (Post)
import Cache exposing (Cache)
import Element exposing (Element, text, el)
import Element.Events as Events

main = Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

type alias Model =
  { current : Page
  , cache : Cache
  }

type Page = Feed | Reading Post

init : () -> (Model,Cmd Msg)
init _ = (  { current = Feed
            , cache = Cache.empty
            }
         , Post.get_all Recv_Posts
         )

-- UPDATE
type Msg
  = Read Post
  | Goto_Feed
  | Recv_Posts (Result Http.Error (List Post))
  | Recv_Post (Result Http.Error Post)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
    Read post -> ({ model
                  | cache = save model model.cache
                  , current = Reading post
                  }
                 , Cmd.none
                 )
    Goto_Feed -> ({ model
                  | cache = save model model.cache
                  , current = Feed
                  }
                 , Post.get_all Recv_Posts
                 )
    Recv_Posts result ->
      case result of
        Ok posts -> ({ model
                     | cache = Cache.add_multiple posts model.cache
                     }
                    , Cmd.none
                    )
        Err e -> (model,Cmd.none)

    Recv_Post result ->
      case result of
        Ok post -> ({ model | cache = Cache.add post model.cache }, Cmd.none)
        Err e -> (model, Cmd.none)

save : Model -> Cache -> Cache
save model cache
  = case model.current of
      Feed -> cache
      Reading post -> Cache.add post cache

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

-- VIEW
view : Model -> Browser.Document Msg
view model =
  { title = case model.current of
      Feed -> "CSDUMMI.XYZ"
      Reading post -> post.title ++ " | CSDUMMI.XYZ"
  , body = [Element.layout [] <|
      Element.column [Element.centerX]
        [ navigation
        , case model.current of
            Feed -> Element.column [] <| Cache.view (Post.view Read) model.cache
            Reading post -> Post.read post
        , text "(C) Joris Gutjahr. Licensed under GPL-3"
        ]]
  }


navigation : Element Msg
navigation =
  Element.wrappedRow [Element.padding 5]
    [ el [Events.onClick Goto_Feed] <| text "Feed"
    ]
