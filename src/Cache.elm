module Cache exposing
  ( Cache
  , empty
  , add
  , add_multiple
  , remove
  , view
  )
import Dict exposing (Dict)
import Post exposing (Post)
import Element exposing (Element)

type alias Cache = Dict Int Post

empty : Cache
empty = Dict.empty

add : Post -> Cache -> Cache
add post
 = Dict.insert post.id post

add_multiple : List Post -> Cache -> Cache
add_multiple posts cache
 = List.foldl (\p -> Dict.insert p.id p) cache posts

remove : Int -> Cache -> Cache
remove id
  = Dict.remove id

view : (Post -> Element msg) -> Cache -> List (Element msg)
view fn = List.map fn << Dict.values
