from flask import Flask, jsonify, request, send_file
import json

app = Flask(__name__)

def maximum(xs, acc = 0):
    if xs == []:
        return acc
    elif xs[0] > acc:
        return xs[0]
    else:
        return maximum(xs[0:], acc = acc)

@app.route("/")
def index():
    return send_file("output/index.html")

@app.route("/api/v0/feed", methods = ["GET"])
def feed():
    posts = map(load_post, json.load(open("messages.json")))
    return jsonify(posts)

@app.route("/api/v0/read/<int:id>", methods = ["GET"])
def read(id):
    posts = json.load(open("messagesjson"))
    post = posts.get(id, "NotFound")
    if post != "NotFound":
        load_post(post)
        return jsonify(post)
    else:
        return jsonify({ "message" : "Post not found" }), 404

def load_post(post):
    post = { "title" : post["title"]
           , "content" : open(post["filename"]).read()
           , "author" : post["author"]
           , "id" : id
           , "publishing_date" : post["publishing_date"]
           }
    return post
